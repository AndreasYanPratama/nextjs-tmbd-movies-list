import Link from 'next/link';
import { useRouter } from "next/router";
import { useState } from "react";

export default function MainLayout({ children }) {
  const router = useRouter();
  const [dropdown, setDropdown] = useState(false);

    return (
      <div className="min-h-screen overflow-x-hidden overflow-y-auto bg-gray-800">
        <nav className="shadow-md bg-black text-white z-30 fixed top-0 right-0 w-full h-16 flex justify-start items-center">
          <div className="container mx-auto">
            <div className="flex justify-between items-center">
              <div className="font-bold text-2xl text-red-500">LogoIpsum</div>
              <div className="grid grid-cols-3 gap-2 mobile:hidden">
                <div className="text-right">
                  <Link href="/">
                    <a className={`${router.pathname == "/" ? "bg-red-500 rounded" : ""} p-3`} >Now Playing</a>
                  </Link>
                </div>
                <div className="text-right">
                  <Link href="/upcoming">
                    <a className={`${router.pathname == "/upcoming" ? "bg-red-500 rounded" : ""} p-3`} >Up Coming</a>
                  </Link>
                </div>
                <div className="text-right">
                  <a className="p-3 rounded">Sign In</a>
                </div>
              </div>
              {/* mobile */}
              <div className="justify-end ml-auto relative px-2 sm:hidden">
                <svg onClick={()=> setDropdown(!dropdown)} xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 cursor-pointer" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth={2}>
                  <path strokeLinecap="round" strokeLinejoin="round" d="M4 6h16M4 12h16M4 18h16" />
                </svg>
                <div id="dropdown" className={`${dropdown ? '' : 'hidden'} absolute right-0 z-10 bg-gray-500 divide-y divide-gray-100 rounded shadow w-32`}>
                  <ul className="py-1 text-sm text-[#f9f9f9] border-white hover:bg-gray-600 rounded" aria-labelledby="dropdownDefault">
                    <li className="mx-2 my-4">
                      <Link href="/">
                        <a className={`${router.pathname == "/" ? "bg-red-500 rounded" : ""} p-1`} >Now Playing</a>
                      </Link>
                    </li>
                    <li className="mx-2 my-4">
                      <Link href="/upcoming">
                        <a className={`${router.pathname == "/upcoming" ? "bg-red-500 rounded" : ""} p-1`} >Up Coming</a>
                      </Link>
                    </li>
                    <li className="mx-2 mt-5 mb-2">
                      <a className="p-3 rounded">Sign In</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </nav>
        {children}
        <footer className="bg-gray-800 h-20 text-gray-400">
          <div className="container mx-auto h-full py-3">
            <div className="text-center h-full flex justify-center items-center">
              &copy; Andreas Yan Pratama - 2021
            </div>
          </div>
        </footer>
      </div>
    )
  }
  