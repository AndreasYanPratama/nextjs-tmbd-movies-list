export const CONFIG = {
    KEY: 'a226fc5f2b6941df49be678c6dda5db0',
    BASE_URL: 'https://api.themoviedb.org/3/',
    BASE_IMAGE_URL: 'https://image.tmdb.org/t/p/original/',
    DEFAULT_LANGUAGE: 'en-us',
};