import { format_date_1 } from '../../../helpers/datetime';
import { genre_list } from '../../../helpers/movie';
import { CONFIG } from '../../../config/api';

export default function MovieItemDetail({ ...movie }){
    console.log(movie.backdrop_path)
    return(
        <div className="w-full h-auto overflow-hidden">
            <div className="grid grid-cols-6 gap-3">
                <img src={`${CONFIG.BASE_IMAGE_URL}/${movie.backdrop_path}`} className="w-full rounded col-span-6" alt="" />
                <div className="w-full col-span-6">
                    <div>
                        <span className="text-lg text-gray-400 my-2">
                            {format_date_1(movie.release_date)}
                        </span>
                        <div className="flex items-center">
                            <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 text-yellow-400" viewBox="0 0 20 20" fill="currentColor">
                                <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                            </svg>
                            <span className="text-white mx-2">
                                {(movie.vote_average).toFixed(2)}/10
                            </span>
                        </div>
                        <div className="text-lg text-gray-400">
                            Genre : {genre_list(movie.genres)}
                        </div>
                        <p className="text-lg break-words w-full h-auto text-white">
                            {movie.overview}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    )
}