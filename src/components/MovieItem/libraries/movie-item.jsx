import Link from 'next/link';
import { CONFIG } from "../../../config/api";
import { format_date } from '../../../helpers/datetime';

export default function MovieItem({...movie}){
    return(
        <div className="border rounded w-full overflow-hidden p-3">
            <div className="grid">
                <div className="w-full">
                    <Link href={`/detail/${movie.id}`}>
                        <img src={`${CONFIG.BASE_IMAGE_URL}/${movie.poster_path}`} alt="movie-thumbnail	&empty;" className="w-full rounded cursor-pointer" />
                    </Link>
                </div>
                <div className="text-white">
                    <h1 className="font-bold text-base">
                        <Link href={`/detail/${movie.id}`} className="hover:text-gray-600">
                            <span className="mobile:hidden">
                                {movie.title.length > 20 ? `${movie.title.substr(0, 20)}...` : movie.title}
                            </span>
                        </Link>
                        <Link href={`/detail/${movie.id}`} className="hover:text-gray-600">
                            <span className="sm:hidden">
                                {movie.title.length > 50 ? `${movie.title.substr(0, 50)}...` : movie.title}
                            </span>
                        </Link>
                    </h1>
                    <div>
                        <span className="inline-block text-xs my-2">{format_date(movie.release_date)}</span>
                    </div>
                </div>
            </div>
        </div>
    )
}