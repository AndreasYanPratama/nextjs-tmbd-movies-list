import Head from 'next/head'
import { useState, useEffect } from 'react';
import ReactPaginate from 'react-paginate';

import MovieItem from '../components/MovieItem';
import MovieRepo from '../libraries/repositories/movies';

import MainLayout from '../layout/main';

var halaman = 1;

export async function getServerSideProps(halaman) {
  const { results, total_pages } = await MovieRepo.fetchNowPlaying(halaman);

  // console.log(results);
  return {
    props: {
      movies: results,
      all: total_pages
    }
  }
}

export default function Home({ movies, all }){
  const [offset, setOffset] = useState(1);
  const [movies_item, setMovies] = useState(movies);

  const doMovies = async (offset) => {
    try{
        const { results } = await MovieRepo.fetchNowPlaying(offset);
        var m_temp = results;
        // console.log(m_temp);
        setMovies(m_temp)
    }catch (error){
        console.log(error);
    }
  };

  const handlePageChange = async (selectedPage) => {
    setOffset(selectedPage.selected+1);
  };

  useEffect(() =>{
    // console.log(offset)
    doMovies(offset);
  },[offset])

  return(
    <MainLayout>
      <Head>
        <title>Movies Catalogue - NextJS</title>
      </Head>
      <div className="container mx-auto min-h-screen pt-16 mb-12">
        <div className="my-8">
          <h1 className="text-4xl text-white">Now Playing Movies</h1>
        </div>
        <div className="grid gap-3 mobile:grid-cols-1 sm:grid-cols-3 lg:grid-cols-5">
          {
            movies_item.map((movie) => (
              <MovieItem key={movie.id} {...movie} />
            ))
          }
        </div>
      </div>
      {/* pagination */}
      <div className="w-full bg-gray-800 mt-3">
        <div className="flex items-center justify-center">
          <ReactPaginate
            containerClassName="flex text-gray-200"
            activeClassName="font-bold bg-gray-900 text-red-500 pointer-events-none"
            previousClassName="mx-1 px-3 py-2 bg-gray-200 text-gray-700 hover:bg-gray-700 hover:text-gray-200 rounded-lg"
            nextClassName="mx-1 px-3 py-2 bg-gray-200 text-gray-700 hover:bg-gray-700 hover:text-gray-200 rounded-lg"
            pageClassName="mx-1 px-3 py-2 bg-gray-200 text-gray-700 hover:bg-gray-700 hover:text-gray-200 rounded-lg"
            initialPage={0}
            pageCount={all}
            onPageChange={(selectedItem) => handlePageChange(selectedItem)}
          />
        </div>
      </div>
    </MainLayout>
  );
}
