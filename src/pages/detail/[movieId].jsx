import Head from "next/head";
import { useEffect } from "react";

import MovieRepo from '../../libraries/repositories/movies';
import MainLayout from "../../layout/main/libraries/main";
import MovieItemDetail from "../../components/MovieItemDetail";

import Router from 'next/router';
import Breadcrumb from '../../components/Breadcrumb';

export async function getServerSideProps(context){
    const { params: { movieId } } = context;
    const movie =await MovieRepo.fetchDetailMovie(movieId);
    return {
        props: {
            movie,
        }
    }
}

export default function MovieDetail({
    movie,
}){
    const pages = [
        { name: movie.title, current: true}
    ]
    return(
        <MainLayout>
            <Head>
                <title>Movies Catalogue - NextJS - Detail</title>
            </Head>
            <div className="container mx-auto mon-h-screen pt-24 mb-12">
                <Breadcrumb pages={pages} />
                <div className="my-8 text-white flex justify-center">
                    <h2 className="text-4xl font-bold">
                        {movie.title}
                    </h2>
                </div>
                <div className="flex justify-start items-start flex-col">
                    <MovieItemDetail {...movie} />
                </div>
            </div>
        </MainLayout>
    );
}