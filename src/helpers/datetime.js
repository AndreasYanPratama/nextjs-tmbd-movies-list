import moment from 'moment';

export const format_date = (dateString) => {
    let myDate = new Date(dateString);
    return (moment(myDate).format("YYYY"));
}

export const format_date_1 = (dateString) => {
    let myDate = new Date(dateString);
    return (moment(myDate).format("DD MMM YYYY"));
}